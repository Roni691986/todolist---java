package com.test.roni;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/TodoServlet")
public class Todo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static DAO dao = new DAO();
	static HttpSession session;
	static ArrayList<Item> listOfItems;
	String id,date,note,status;

	public static void getDataFromDB(HttpServletRequest request) {
		listOfItems = dao.getItems();
		session = request.getSession();
		session.setAttribute("listOfItems", listOfItems);
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		response.setContentType("text/html");
		session = request.getSession();
		Boolean paramValidations = checkParamsValidation(request);
		if (paramValidations) {
			if (action != null) {
				switch(action) {
					case "addItem" :
						 date = request.getParameter("date");
						 note = request.getParameter("note");
						if(dao.addItem(date,note)) {
							System.out.println("addItem: " + note);
						}
						System.out.println("date" + date);
						break; 
					case "deleteItem" :
						 id = request.getParameter("id");
						 System.out.println("id"+id);
						if(dao.deleteItem(id))
							System.out.println("deleteItem: " + id);
						break; 
					case "updateItem" :
						 id = request.getParameter("id");
						 note = request.getParameter("note");
						if(dao.updateItem(id,note)) {
							System.out.println("updateItem: " + id);
						}
						break; 
					case "doneItem" :
						 id = request.getParameter("id");
						 status = request.getParameter("status");
						if(dao.doneItem(id,status)) {
							System.out.println("doneItem: " + id);
						}
						break; 
				}
			}
			
		}
		
		listOfItems = dao.getItems();
		session.setAttribute("listOfItems", listOfItems);
		request.getRequestDispatcher("index.jsp").forward(request, response);
		
	}
	private Boolean checkParamsValidation(HttpServletRequest request) {
		Enumeration keys = request.getParameterNames();
		while (keys.hasMoreElements() )
		{
			String key = (String)keys.nextElement();

			if (key == null || key.length() == 0 || key.equals("")){
				return false;
			}
		}
		return true;
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request,response);
	}

}
